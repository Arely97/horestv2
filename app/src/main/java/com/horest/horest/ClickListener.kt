package com.horest.horest

import android.view.View

interface ClickListener {
    fun onClick(vista: View, index:Int)
}