package com.horest.horest

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_dato_h.*
import kotlinx.android.synthetic.main.activity_restaurante2.*

class Restaurante2 : AppCompatActivity() {
    var listaH: RecyclerView? = null
    var adaptador: AdaptadorCustom4? = null
    var layoutManager: RecyclerView.LayoutManager? = null  //el diseño

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurante2)
        val tvUsuario: TextView = findViewById(R.id.NameUs5)

        val usuario: String = intent.extras!!.getString("USUARIO").toString()
        tvUsuario.text = usuario
        salir5.setOnClickListener {
            if (usuario == "Google") {
                Firebase.auth.signOut()
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, Login::class.java))
                finish()
            }
            startActivity(Intent(this, Login::class.java))
        }
        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val tipo = bundleLibriDeNull.getInt("key_tipo")
            if (tipo == 1) {
                val Restaurante12 = ArrayList<ListR2>()

                Restaurante12.add(
                        ListR2(
                                "Restaurant Las Iguanas.",
                                "Carretera Misantla-Xalapa Km 1.700 Teléfono: 235-32-3-09-09.",
                                3.5F,
                                R.drawable.rest_iguanas
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant Los Delfines",
                                "Carretera Misantla-Tenochtitlán Km 1.0 Teléfono: (045) 235-105-57-01.",
                                3.5F,
                                R.drawable.rest_delfines
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant México Lindo",
                                "Boulevard Ávila Camacho, número 203 Teléfono: 235-32-3-49-02.",
                                3.5F,
                                R.drawable.rest_mexicolindo
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant Los cafetos",
                                "Calle Morelos, Colonia Centro Teléfono: 01-235-32-3-50-62.",
                                3.5F,
                                R.drawable.rest_cafetos
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant Totonacapan",
                                "Calle Degollado, número 116, Colonia Centro Teléfono: 01-235-32-3-10-28.",
                                3.5F,
                                R.drawable.rest_totonacapan
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant-Bar La Herradura",
                                "Avenida Morelos No. 106 Letra A, colonia Centro Teléfono: 235-32-3-05-42.",
                                3.5F,
                                R.drawable.rest_herradura
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant-Bar Vista Hermosa.",
                                "Carretera Misantla-Xalapa Km 2 Teléfono: (045) 235-107-43-45.",
                                3.5F,
                                R.drawable.rest_vista_hermosa
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant-Bar La Ostra del Golfo",
                                "Calle Aquiles Serdán, s/n, Colonia Centro Teléfono: 01-235-32-3-00-60\t",
                                3.5F,
                                R.drawable.rest_ostra_del_golfo
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant-Bar El Quelite",
                                "Calle Alfonso Arroyo Flores, s/n, Colonia Centro Teléfono: 01-235-3-14-33",
                                3.5F,
                                R.drawable.rest_quelite
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Restaurant Tiburones Rojos",
                                "Avenida Ávila Camacho, s/n, Colonia Centro Teléfono: (045) 235-101-58-68.",
                                3.5F,
                                R.drawable.rest_tiburones_rojos
                        )
                        )
                listaH = findViewById(R.id.ListaRC9)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista
                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                listaH?.layoutManager = layoutManager  // donde se dibuje el layout


                adaptador = AdaptadorCustom4(this, Restaurante12, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        val bundle = Bundle()
                        bundle.apply {
                            putInt("key_index", index)
                            putInt("key_tipo", tipo)
                            putString("USUARIO", usuario)
                        }
                        val intent = Intent(applicationContext, Restaurante4::class.java).apply {
                            putExtras(bundle)
                        }
                        startActivity(intent)
                    }
                })

                listaH?.adapter = adaptador

            }
            else if (tipo == 2) {
                val Restaurante12 = ArrayList<ListR2>()

                Restaurante12.add(
                           ListR2(
                                   "Cocina Económica  La Fondita",
                                   "Calle Bojalil Gil, número 112, Colonia Centro Teléfono: 235-32-3-50-62",
                                   3.5F,
                                   R.drawable.rest_lafondita
                           )
                           )
                Restaurante12.add(
                        ListR2(
                                "Cocina Económica La Sopa",
                                "Calle Comomfort esquina Mina, número 123, Colonia Centro Teléfono: 01-235-32-3-50-23",
                                3.5F,
                                R.drawable.rest_lasopa
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Venezia Pizza Pasta y Vino",
                                "Calle Bojalil Gil, número 112, Colonia Centro Teléfono: 01-235-32-3-50-62",
                                3.5F,
                                R.drawable.rest_venecio
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Pizzas Angelotti",
                                "Calle Camilo González, número 108, Colonia Centro",
                                3.5F,
                                R.drawable.rest_angelotti
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Pizzería Stefani",
                                "Calle Comonfort Número 121, Colonia Centro",
                                3.5F,
                                R.drawable.rest_pizzer_a_stefani
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Pollos Dany",
                                "Calle Francisco I. Madero esquina Bojalil Gil, número 200, Colonia Centro Sucursal Boulevard Ávila Camacho, número 712.",
                                3.5F,
                                R.drawable.rest_pollodany
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Refresquería El Kiosco",
                                "Parque Morelos, Col. Centro",
                                3.5F,
                                R.drawable.rest_el_kiosco
                        )
                )
                listaH = findViewById(R.id.ListaRC9)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista
                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                listaH?.layoutManager = layoutManager  // donde se dibuje el layout


                adaptador = AdaptadorCustom4(this, Restaurante12, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        val bundle = Bundle()
                        bundle.apply {
                            putInt("key_index", index)
                            putInt("key_tipo", tipo)
                            putString("USUARIO", usuario)
                        }
                        val intent = Intent(applicationContext, Restaurante4::class.java).apply {
                            putExtras(bundle)
                        }
                        startActivity(intent)
                    }
                })

                listaH?.adapter = adaptador

            }
            else if (tipo == 3) {
                val Restaurante12 = ArrayList<ListR2>()

                Restaurante12.add(
                        ListR2(
                                "Restaurant Familiar  El Quelite Fiesta",
                                "Avenida 5 de Mayo, número 204, Colonia Centro Teléfono: 01-235-32-3-04-13",
                                3.5F,
                                R.drawable.rest_quelite_2
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Hotel Campestre Casa Blanca",
                                "Carretera Misantla- Tenochtitlán Km. 1.0 Teléfono: 235-32-3-46-14",
                                3.5F,
                                R.drawable.rest_casa_blanca
                        )
                )

                listaH = findViewById(R.id.ListaRC9)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista
                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                listaH?.layoutManager = layoutManager  // donde se dibuje el layout


                adaptador = AdaptadorCustom4(this, Restaurante12, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        val bundle = Bundle()
                        bundle.apply {
                            putInt("key_index", index)
                            putInt("key_tipo", tipo)
                            putString("USUARIO", usuario)
                        }
                        val intent = Intent(applicationContext, Restaurante4::class.java).apply {
                            putExtras(bundle)
                        }
                        startActivity(intent)
                    }
                })

                listaH?.adapter = adaptador

            }
            else if (tipo == 4) {
                val Restaurante12 = ArrayList<ListR2>()
                Restaurante12.add(
                        ListR2(
                                "Coctelería La Ostra",
                                "Calle Zaragoza, número 104, Colonia Centro",
                                3.5F,
                                R.drawable.rest_ostra_del_golfo2
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Coctelería El Calamar",
                                "Calle Zaragoza, número 103, Colonia Centro",
                                3.5F,
                                R.drawable.rest_cocteler_a_el_calamar
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Coctelería Tiburones Rojos",
                                "Avenida Ávila Camacho, s/n, Colonia Centro",
                                3.5F,
                                R.drawable.rest_tiburones_rojos
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Buffet El Taco Mexicano",
                                "Colonia Teresita Peñafiel, s/n",
                                3.5F,
                                R.drawable.rest_tacomexicano
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Las Gordas Antojitos",
                                "Calle Juárez esquina Salvador Díaz Mirón, s/n, Colonia Centro y sucursal en calle Alfonso Arroyo Flores esquina 5 Mayo. ",
                                3.5F,
                                R.drawable.rest_gordas
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Antojitos Tere",
                                "Esquina Bojalil Gil",
                                3.5F,
                                R.drawable.rest_antojitos_tere
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Cenaduría Lita",
                                "Calle Lerdo, número 125, Colonia Centro",
                                3.5F,
                                R.drawable.rest_cenadur_a_lita
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Cenaduría Magui",
                                "Calle Hidalgo, s/n, Colonia Centro",
                                3.5F,
                                R.drawable.rest_cenadur_a_magui
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Asadero",
                                " Linda Vista Carretera Misantla-Martinez de la Torre",
                                3.5F,
                                R.drawable.rest_asadero_linda_vista
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería La Chiquitita",
                                "Boulevard Ávila Camacho con calle Rosendo Álvarez, s/n, Colonia Centro",
                                3.5F,
                                R.drawable.rest_taqueirabj
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería BJ",
                                "Calle Nizin, número 164, Colonia Benito Juárez.",
                                3.5F,
                                R.drawable.rest_taqueirabj
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería Raquel",
                                "Calle Zaragoza, número 102, Colonia Centro",
                                3.5F,
                                R.drawable.rest_raquel
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería El Negro ",
                                "Boulevard Ávila Camacho esquina Dante Delgado, s/n.",
                                3.5F,
                                R.drawable.rest_taqueiranegro
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería Lulú",
                                "Calle Xalapa, número 117, Colonia Centro",
                                3.5F,
                                R.drawable.rest_taquer_a_lul_
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería El Güero",
                                "Calle Justa García Katz, Colonia Benito Juárez",
                                3.5F,
                                R.drawable.rest_taquer_a_el_g_ero
                        )
                )
                Restaurante12.add(
                        ListR2(
                                "Taquería Vielo´s",
                                "Boulevard Ávila Camacho, número 434, Colonia Centro",
                                3.5F,
                                R.drawable.rest_taquer_a_vielo_s
                        )
                )


                listaH = findViewById(R.id.ListaRC9)
                listaH?.setHasFixedSize(true)  //adaptador tamaño de la vista
                layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
                listaH?.layoutManager = layoutManager  // donde se dibuje el layout


                adaptador = AdaptadorCustom4(this, Restaurante12, object : ClickListener {
                    override fun onClick(vista: View, index: Int) {
                        val bundle = Bundle()
                        bundle.apply {
                            putInt("key_index", index)
                            putInt("key_tipo", tipo)
                            putString("USUARIO", usuario)
                        }
                        val intent = Intent(applicationContext, Restaurante4::class.java).apply {
                            putExtras(bundle)
                        }
                        startActivity(intent)
                    }
                })

                listaH?.adapter = adaptador

            }

        }
        creditos6.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }

    }
}
