package com.horest.horest

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdaptadorCustom4(var context:Context, items:ArrayList<ListR2>, var listener: ClickListener): RecyclerView.Adapter<AdaptadorCustom4.ViewHolder>() {

    var items:ArrayList<ListR2>? = null
    init {
        this.items = items
    }

    // el archivo xml en viewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorCustom4.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.activity_list_r2,parent,false)
        val viewHolder = ViewHolder(vista,listener)

        return viewHolder
    }

    override fun onBindViewHolder(holder: AdaptadorCustom4.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.foto?.setImageResource(item?.foto!!)
        holder.nombre?.text = item?.nombre
        holder.direccion?.text = item?.direccion
        holder.rating?.rating = item?.rating!!
    }

    override fun getItemCount(): Int {
        return items?.count()!!
    }

    class ViewHolder(vista: View, listener: ClickListener):RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var foto:ImageView? = null
        var nombre:TextView? = null
        var direccion:TextView? = null
        var rating:RatingBar? = null
        var listener:ClickListener? = null
        init {
            foto = vista.findViewById(R.id.foto2)
            nombre = vista.findViewById(R.id.nombre2)
            direccion = vista.findViewById(R.id.direccion2)
            rating = vista.findViewById(R.id.rtBar2)
            this.listener = listener
            vista.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.listener?.onClick(v!!,adapterPosition)
        }

    }
}