package com.horest.horest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView

class Splash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //forza el tamaño completo del telefono
        window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splash)
        Thread.sleep(500)
            val texto1: TextView = findViewById(R.id.tema)
            val texto2: TextView = findViewById(R.id.tema2)
            val texto3: TextView = findViewById(R.id.tema3)

            //Asignacioón de las animaciones
            val anim1: Animation = AnimationUtils.loadAnimation(this, R.anim.abajo)
            texto1.startAnimation(anim1)
            val anim2: Animation = AnimationUtils.loadAnimation(this, R.anim.abajo)
            texto2.startAnimation(anim2)
            val anim3: Animation = AnimationUtils.loadAnimation(this, R.anim.abajo)
            texto3.startAnimation(anim3)
            //Tiempo de espera para el cambio de venta
            Handler().postDelayed({
                startActivity(Intent(this, Login::class.java))
                finish()
            }, 5000)

    }
}