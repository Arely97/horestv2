package com.horest.horest

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_dato_h.*

class DatoH : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dato_h)
        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val index = bundleLibriDeNull.getInt("key_index")
            val tipo = bundleLibriDeNull.getInt("key_tipo")
            val tvUsuario: TextView = findViewById(R.id.NameUs3)
            val usuario: String = intent.extras!!.getString("USUARIO").toString()
            tvUsuario.text = usuario
            salir3.setOnClickListener {
                if (usuario == "Google") {
                    Firebase.auth.signOut()
                    FirebaseAuth.getInstance().signOut()
                    startActivity(Intent(this, Login::class.java))
                    finish()
                }
                startActivity(Intent(this, Login::class.java))
            }
            if (tipo == 1) {
                val hoteles = ArrayList<Hotel2>()
                hoteles.add(
                    Hotel2(
                        "Hotel Imperial",
                        "Con 8 habitaciones disponibles.",
                        "Calle cuña, número 03, colonia centro.",
                        "Baño, agua caliente, lavabo, clima y televisión por cable e internet.",
                        "Tiempo en funcionamiento: 40 años & Propietario: Lic. Julio Rafael Flores Rodríguez.",
                        R.drawable.hotel_imperial
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Misantla",
                        "Con 16 habitaciones disponibles.",
                        "Avenida Carlos Salinas de Gortari, número 223, colonia Plan de la Vieja. Teléfono: 235-32-3-19-35",
                        "Clima, televisión por cable, internet inalámbrico, cuadros típicos de la Ciudad de Misantla, agua caliente, lavandería, servicio de cafetería y estacionamiento.",
                        "Tiempo en funcionamiento: 5 años & Propietario: Rosa Bertha Cruz Teddy.",
                        R.drawable.hotel_misantla
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Dorado Amanecer",
                        "Con 17 habitaciones disponibles.",
                        "Boulevard Ávila Camacho, número 304 Teléfono: 235-32-3-49-02.",
                        "Internet inalámbrico, lavandería, clima, restaurant, estacionamiento, televisión con cable y agua caliente",
                        "Tiempo en funcionamiento: 2 Años y 11 Meses.",
                        R.drawable.hotel_dorado_amanecer
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel León",
                        "Con 26 habitaciones disponibles.",
                        "Calle Degollado esquina Francisco I. Madero, número 114, colonia Centro. Teléfono: 235-32-3-00-72.",
                        "Agua caliente y fría, televisión por cable, internet y aire acondicionado.",
                        "Tiempo en funcionamiento: Desde 1972, 40 años & Propietario: Primer propietario el Señor León Arroyo Castellanos y la actual propietaria Ema García.",
                        R.drawable.hotel_leon
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Don Pablo",
                        "Con 28 habitaciones disponibles.",
                        "Avenida 5 de Mayo, número 202, colonia Centro Teléfono: 235-32-3-18-55",
                        "Aire acondicionado, televisión con cable, internet inalámbrico, teléfono, agua fría y caliente las 24 horas, servicio de restaurant, estacionamiento y alberca.",
                        "Tiempo en funcionamiento: 9 años & Propietario: Raquel Lavalle Trubell",
                        R.drawable.hotel_don_pablo
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Claudia Isabel",
                        "Con 23 habitaciones disponibles.",
                        "Calle Degollado, número 116, colonia Centro. Teléfono: 235-32-3-16-05.",
                        "Agua caliente y fría, teléfono, internet, aire acondicionado, estacionamiento, cable.",
                        "Tiempo en funcionamiento: 8 años & Propietario: Guillermo Hernández Díaz",
                        R.drawable.hotel_claudia_isabel
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Posada del Ángel",
                        "Con 14 habitaciones disponibles.",
                        "Calle Hidalgo, número 127, colonia Centro. Teléfono: 235-32-3-40-75.",
                        "Agua caliente y fría, televisión con cable, internet y aire acondicionado, estacionamiento.",
                        "Tiempo en funcionamiento: 9 años & Propietario: Guadalupe Gutiérrez Pérez",
                        R.drawable.hotel_posada_del_angel
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Delfos",
                        "Con 13 habitaciones disponibles.",
                        "Calle Úrsulo Galván, número 103, colonia Centro.",
                        "Agua caliente y fría, televisión con cable, internet, aire acondicionado y estacionamiento.",
                        "Tiempo en funcionamiento: 30 años & Propietario: Enrique Rodríguez Méndez",
                        R.drawable.hotel_delfos
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Gilbon",
                        "Con 23 habitaciones disponibles.",
                        "Calle Degollado",
                        "",
                        "",
                        R.drawable.hotel_gilbon
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Hotel Sol ",
                        "Con 18 habitaciones disponibles.",
                        "Calle Morelos Col. Centro.",
                        "",
                        "",
                        R.drawable.hotel_sol
                    )
                )

                NombreHotel.setText(hoteles.get(index).NombreHotel)
                DireTelefono.setText(hoteles.get(index).DireTelefono)
                Servicio.setText(hoteles.get(index).Servicio)
                AntiPro.setText(hoteles.get(index).AntiPro)
                Habitacion.setText(hoteles.get(index).Habitacion)
                imageHotel.setImageResource(hoteles.get(index).foto)
            }
            else if (tipo == 2) {
                val hoteles = ArrayList<Hotel2>()
                hoteles.add(
                    Hotel2(
                        "Hotel Campestre Casa Blanca",
                        "20 habitaciones de tipo cabañas y bungalows.",
                        "Carretera Misantla- Tenochtitlán Km. 1.0 Teléfono: 235-32-3-46-14.",
                        "Agua caliente y fría, televisión por cable, internet, aire acondicionado,restaurant, albercas, juegos infantiles, cancha de futbol, pista de correr gimnasio, tirolesa y tours.",
                        "Tiempo en funcionamiento: 3 años & Propietario: Lic. Rafael Salazar García.",
                        R.drawable.hotel_campestre_casa_blanca
                    )
                )
                NombreHotel.setText(hoteles.get(index).NombreHotel)
                DireTelefono.setText(hoteles.get(index).DireTelefono)
                Servicio.setText(hoteles.get(index).Servicio)
                AntiPro.setText(hoteles.get(index).AntiPro)
                Habitacion.setText(hoteles.get(index).Habitacion)
                imageHotel.setImageResource(hoteles.get(index).foto)
            }
            else if (tipo == 3) {
                val hoteles = ArrayList<Hotel2>()
                hoteles.add(
                    Hotel2(
                        "Hotel Hacienda Prom",
                        "Con 25 habitaciones y 3 salones de eventos 15,50,100 personas.",
                        "Carretera Misantla-Xalapa Km 1.700 Teléfono: 235-32-3-09-09",
                        "Agua caliente y fría, televisión con cable, internet, aire acondicionado, estacionamiento,restaurant, room service, salón para eventos, juegos infantiles.",
                        "Tiempo en funcionamiento: 3 años & Propietario: C.P.A Ma. Dolores Prom y Rodríguez",
                        R.drawable.hotel_hacienda_prom
                    )
                )
                NombreHotel.setText(hoteles.get(index).NombreHotel)
                DireTelefono.setText(hoteles.get(index).DireTelefono)
                Servicio.setText(hoteles.get(index).Servicio)
                AntiPro.setText(hoteles.get(index).AntiPro)
                Habitacion.setText(hoteles.get(index).Habitacion)
                imageHotel.setImageResource(hoteles.get(index).foto)
            }
            else {
                val hoteles = ArrayList<Hotel2>()
                hoteles.add(
                    Hotel2(
                        "Auto-hotel Delicias",
                        "Desconocido",
                        "Carretera Misantla- Tenochtitlán Km 1.0",
                        "Desconocido",
                        "Desconocido",
                        R.drawable.auto_hotel_delicias
                    )
                )
                hoteles.add(
                    Hotel2(
                        "Auto hotel Bugambilias",
                        "Desconocido",
                        "Carretera Misantla- Tenochtitlán Km 1.0",
                        "Agua caliente y fría, televisión por cable y aire acondicionado.",
                        "Tiempo en funcionamiento: 8 años y 6 meses & Propietario: Lic. Rafael Salazar García",
                        R.drawable.auto_hotel_secretos
                    )
                )
                NombreHotel.setText(hoteles.get(index).NombreHotel)
                DireTelefono.setText(hoteles.get(index).DireTelefono)
                Servicio.setText(hoteles.get(index).Servicio)
                AntiPro.setText(hoteles.get(index).AntiPro)
                Habitacion.setText(hoteles.get(index).Habitacion)
                imageHotel.setImageResource(hoteles.get(index).foto)
            }
        }
        creditos4.setOnClickListener {
            startActivity(Intent(this, Creditos::class.java))
        }

    }
}