package com.horest.horest

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.google.android.material.tabs.TabLayout

class SliderAdapter : PagerAdapter {
    var context: Context
    lateinit var layoutInflater: LayoutInflater
    var tabLayout: TabLayout? = null

    constructor(context: Context) {
        this.context = context
    }

    //Arreglos
    val slide_images: ArrayList<Int> =
        arrayListOf(R.drawable.hotel, R.drawable.rest, R.drawable.cal)
    val slides_headings: java.util.ArrayList<String> =
        arrayListOf("Hoteles", "Restaurantes", "Elije y califica")
    val slides_descs: java.util.ArrayList<String> = arrayListOf(
        "Descubre los Hoteles en los cuales puedes hospedarte, y clasificados por caracteristicas relevantes",
        "Disfruta de platillos esquisitos en los lugares recomendados de esta ciudad.",
        "Califica el servicio de cada lugar al que visites"
    )

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as RelativeLayout
    }

    override fun getCount(): Int {
        return slides_headings.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view: View = layoutInflater.inflate(R.layout.activity_slider_adapter, container, false)

        var slideImageView: ImageView = view.findViewById(R.id.imaagen) as ImageView
        var slideHeading: TextView = view.findViewById(R.id.titulo1) as TextView
        var slideDesc: TextView = view.findViewById(R.id.titulo2) as TextView

        slideImageView.setImageResource(slide_images[position])
        slideHeading.text = slides_headings[position]
        slideDesc.text = slides_descs[position]
        container.addView(view)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}